using System.Collections.Generic;
using UnityEngine;

public class Building
{
    public List<Building> _inputs = new();

    public List<Building> _outputs = new();

    public readonly BuildingScriptableObject BuildingScriptableObject;
    public readonly ProductScriptableObject ProductScriptableObject;
    
    private bool _hasWaitingProduct;
    private int _maxSupply = 10;
    private int _supply = 0;
    private int _demand = 0;
    public Vector3Int position;

    public Building(BuildingScriptableObject buildingScriptableObject, ProductScriptableObject productScriptableObject)
    {
        BuildingScriptableObject = buildingScriptableObject;
        ProductScriptableObject = productScriptableObject;
        _maxSupply = buildingScriptableObject.maxSupply;
    }

    public Building(SaveData.BuildingData saveDataBuilding)
    {
        BuildingScriptableObject = saveDataBuilding.buildingScriptableObject;
        ProductScriptableObject = saveDataBuilding.productScriptableObject;
        _hasWaitingProduct = saveDataBuilding.hasWaitingProduct;
        _maxSupply = saveDataBuilding.maxSupply;
        _supply = saveDataBuilding.supply;
        _demand = saveDataBuilding.demand;
        position = saveDataBuilding.gridPosition;
    }

    public SaveData.BuildingData ToSaveData()
    {
        var saveBuildingData = new SaveData.BuildingData
        {
            buildingScriptableObject = BuildingScriptableObject,
            productScriptableObject = ProductScriptableObject,
            maxSupply = _maxSupply,
            supply = _supply,
            demand = _demand,
            gridPosition = position,
            inputConnections = new List<Vector3Int>(),
            outputConnections = new List<Vector3Int>()
        };
        foreach (var inputBuilding in _inputs)
        {
            saveBuildingData.inputConnections.Add(inputBuilding.position);
        }
        foreach (var outputBuilding in _outputs)
        {
            saveBuildingData.outputConnections.Add(outputBuilding.position);
        }

        return saveBuildingData;
    }

    public void SellProducts()
    {
        if (_supply > _demand)
        {
            _supply -= 1;
            GameManager.Instance.ChangeMoney(ProductScriptableObject.sellCost);
        }
    }
    public void RemoveProducts(int amount)
    {
        _supply -= amount;
    }

    public void ReceiveProduct()
    {
        if (_hasWaitingProduct && _supply < _maxSupply)
        {
            _supply += 1;
            _hasWaitingProduct = false;
        }
    }

    private void SendProduct()
    {
        if (_supply > 0 && _outputs.Count > 0)
        {
            _outputs[0]._hasWaitingProduct = true;
            _supply -= 1;
        }
    }

    public void MakeProduct()
    {
        if (_supply < _maxSupply)
        {
            _supply += 1;
        }
    }

    public void RequestProduct()
    {
        if (_supply < _maxSupply && _inputs.Count > 0)
        {
            _inputs[0].SendProduct();
        }
    }

    public int GetSupply()
    {
        return _supply;
    }
}