using System;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class MoneyPanel : MonoBehaviour
{
    [SerializeField] private TMP_Text _moneyAmountLabel;

    private void Start()
    {
        GameManager.Instance.moneyPanel = this;
        SetMoney(GameManager.Instance.GetMoney());
    }

    private void OnDestroy()
    {
        if (GameManager.Instance.moneyPanel == this)
        {
            GameManager.Instance.moneyPanel = null;
        }
    }

    public void SetMoney(int money)
    {
        _moneyAmountLabel.text = money.ToString();
    }
}