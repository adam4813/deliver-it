using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class ActiveBuildingMouseFollow : MonoBehaviour
{
    private const float SpriteAlpha = .38f;
    public SpriteRenderer mouseFollowSprite;

    [SerializeField] private Vector3 mouseOffset = new Vector2(8, 16);

    // Start is called before the first frame update
    void Start()
    {
        mouseFollowSprite = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        var gridPosition = GameManager.Instance.GetBuildingTilemapMouseGridPosition();
        transform.position = new Vector3(gridPosition.x + 0.5f, gridPosition.y + 0.5f);
    }

    public void SetImage(Sprite image)
    {
        mouseFollowSprite.sprite = image;
    }

    public void ClearImage()
    {
        mouseFollowSprite.sprite = null;
    }

    public void SetInvalid()
    {
        mouseFollowSprite.color = new Color(1, 0, 0, SpriteAlpha);
    }

    public void SetValid()
    {
        mouseFollowSprite.color = new Color(0, 1, 0, SpriteAlpha);
    }
}