using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RequestItem : MonoBehaviour
{
    [SerializeField] private Image productIcon;

    [SerializeField] private TMP_Text amountDisplay;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetRequestListItem(RequestListItem requestListItem)
    {
        productIcon.sprite = requestListItem.product.icon;
        amountDisplay.text = requestListItem.amount.ToString();
    }
}
