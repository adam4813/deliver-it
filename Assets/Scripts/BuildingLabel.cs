using UnityEngine;
using TMPro;

public class BuildingLabel : MonoBehaviour
{
    [SerializeField]
    private TMP_Text buildingName;
    [SerializeField]
    private TMP_Text buildingLevel;

    [SerializeField] private SpriteRenderer iconSprite;

    public void SetProduct(ProductScriptableObject product)
    {
        iconSprite.sprite = product.icon;
    }
    
    // Start is called before the first frame update
    void Start()
    {
        buildingLevel.text = "1";
    }
    
    public void SetBuildingName(string _buildingName)
    {
        buildingName.text = _buildingName[..2];
    }
}
