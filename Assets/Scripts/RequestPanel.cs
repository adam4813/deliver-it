using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RequestPanel : MonoBehaviour
{
    [SerializeField] private GameObject requestSlotPrefab;
    private Dictionary<RequestSlot, SaveData.RequestListItemData> requestSlots = new();

    private void Start()
    {
        GameManager.Instance.requestPanel = this;
        foreach (var request in  GameManager.Instance.activeRequests)
        {
            CreateRequest(request);
        }
    }

    private void OnDestroy()
    {
        if (GameManager.Instance.requestPanel == this)
        {
            GameManager.Instance.requestPanel = null;
        }
    }

    public void CreateRequest(SaveData.RequestListItemData requestListItem)
    {
        var requestInstance = Instantiate(requestSlotPrefab, transform);
        var requestSlot = requestInstance.GetComponent<RequestSlot>();
        requestSlot.SetRequestScriptableObject(requestListItem._requestScriptableObject);
        requestSlots.Add(requestSlot,requestListItem );
    }

    public void RemoveQuestSlot(RequestSlot requestSlot)
    {
        GameManager.Instance.RemoveRequest(requestSlots[requestSlot]);
        Destroy(requestSlot.gameObject);
    }
}