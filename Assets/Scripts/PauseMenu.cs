using System;
using Eflatun.SceneReference;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private Button _backButton;
    [SerializeField] private Button _saveButton;
    [SerializeField] private Button _quitButton;

    [SerializeField] private SceneReference saveScene;
    [SerializeField] private SceneReference titleScene;
    [SerializeField] private GameObject pausePanel;

    public bool isShown => pausePanel.activeSelf;

    // Start is called before the first frame update
    void Start()
    {
        _quitButton.onClick.AddListener(() =>
        {
            GameManager.Instance.HideMap();
            SceneManager.LoadScene(titleScene.Name);
        });
        _backButton.onClick.AddListener(() =>
        {
            Hide();
            GameManager.Instance.Resume();
        });
        _saveButton.onClick.AddListener(() =>
        {
            GameManager.Instance.HideMap();
            SceneManager.LoadScene(saveScene.Name);
        });
        GameManager.Instance.pauseMenu = this;
    }

    private void OnDestroy()
    {
        if (GameManager.Instance.pauseMenu == this)
        {
            GameManager.Instance.pauseMenu = null;
        }
    }

    public void Show()
    {
        pausePanel.SetActive(true);
    }

    public void Hide()
    {
        pausePanel.SetActive(false);
    }
}