using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuildingToolbar : MonoBehaviour
{
    public List<BuildingScriptableObject> buildingScriptableObjects;
    public List<GameObject> toolbarSlots;

    // Start is called before the first frame update
    void Start()
    {
        if (toolbarSlots.Count < buildingScriptableObjects.Count * GameManager.Instance.products.Count) return;
        for (var i = 0; i < buildingScriptableObjects.Count; i++)
        {
            for (var j = 0; j < GameManager.Instance.products.Count; j++)
            {
                var toolbarIndex = i * GameManager.Instance.products.Count + j;

                var toolbarImage = toolbarSlots[toolbarIndex].GetComponent<Image>();
                toolbarImage.sprite = buildingScriptableObjects[i].image;

                var toolbarButton = toolbarSlots[toolbarIndex].GetComponent<HotbarButton>();
                toolbarButton.Setup(buildingScriptableObjects[i], GameManager.Instance.products[j]);
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
    }
}