using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RequestSlot : MonoBehaviour
{
    [SerializeField] private GameObject requestItemsList;
    [SerializeField] private GameObject requestItemPrefab;
    [SerializeField] private Button deliverButton;

    private RequestScriptableObject _requestScriptableObject;

    private void Start()
    {
        deliverButton.GetComponentInChildren<TMP_Text>().text = $"Deliver for {_requestScriptableObject.reward}";
        deliverButton.onClick.AddListener(() =>
        {
            if (GameManager.Instance.TryCompleteRequest(_requestScriptableObject))
            {
                GameManager.Instance.requestPanel.RemoveQuestSlot(this);
            }
        });
    }

    public void SetRequestScriptableObject(RequestScriptableObject requestScriptableObject)
    {
        _requestScriptableObject = requestScriptableObject;
        foreach (var item in requestScriptableObject.items)
        {
            var requestItemInstance = Instantiate(requestItemPrefab, requestItemsList.transform);
            var requestItem = requestItemInstance.GetComponent<RequestItem>();
           requestItem.SetRequestListItem(item);
        }
    }

    public RequestScriptableObject GetRequestScriptableObject()
    {
        return _requestScriptableObject;
    }
}