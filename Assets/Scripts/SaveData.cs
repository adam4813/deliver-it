using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SaveData
{
    [Serializable]
    public struct BuildingData
    {
        public Vector3Int gridPosition;
        public BuildingScriptableObject buildingScriptableObject;
        public ProductScriptableObject productScriptableObject;
        public bool hasWaitingProduct;
        public int maxSupply;
        public int supply;
        public int demand;
        public List<Vector3Int> inputConnections;
        public List<Vector3Int> outputConnections;
    }

    [Serializable]
    public struct RequestListItemData
    {
        public RequestScriptableObject _requestScriptableObject;
        public int index;
    }

    public int money;
    public List<BuildingData> buildings;
    public List<RequestListItemData> requests;

    public string ToJson()
    {
        return JsonUtility.ToJson(this);
    }

    public void LoadFromJson(string a_Json)
    {
        JsonUtility.FromJsonOverwrite(a_Json, this);
    }
}

public interface ISaveable
{
    void PopulateSaveData(SaveData a_SaveData);
    void LoadFromSaveData(SaveData a_SaveData);
}