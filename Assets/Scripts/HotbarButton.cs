using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HotbarButton : MonoBehaviour
{
    private BuildingScriptableObject _buildingScriptableObject;
    private ProductScriptableObject _productScriptableObject;

    [SerializeField] private Image productIcon;

    [SerializeField] private TMP_Text costDisplay;
    // Start is called before the first frame update
    void Start()
    {
        var button = GetComponent<Button>();
        button.onClick.AddListener(OnClick);
    }

    private void OnClick()
    {
        GameManager.Instance.SetActiveBuilding(_buildingScriptableObject, _productScriptableObject);
    }

    public void Setup(BuildingScriptableObject buildingScriptableObject, ProductScriptableObject productScriptableObject)
    {
        _buildingScriptableObject = buildingScriptableObject;
        _productScriptableObject = productScriptableObject;
        productIcon.sprite = productScriptableObject.icon;
        costDisplay.text = buildingScriptableObject.cost.ToString();
    }
}
