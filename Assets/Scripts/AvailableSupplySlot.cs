using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AvailableSupplySlot : MonoBehaviour
{
    [SerializeField] private Image productIcon;

    [SerializeField] private TMP_Text valueDisplay;
    private ProductScriptableObject _productScriptableObject;

    public void SetProduct(ProductScriptableObject productScriptableObject)
    {
        _productScriptableObject = productScriptableObject;
        productIcon.sprite = productScriptableObject.icon;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        valueDisplay.text = GameManager.Instance.GetAvailableProductQuantity(_productScriptableObject).ToString();
    }
}
