﻿using System;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "Building", menuName = "Tiles/Building", order = 1)]
public class BuildingScriptableObject : ScriptableObject
{
    public string buildingName;
    public Sprite image;
    public Tile tile;
    public int cost;
    public bool isFactory;
    public bool isStore;
    public bool isWarehouse;
    public int maxSupply = 10;
}