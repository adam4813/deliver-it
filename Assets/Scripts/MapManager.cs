using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Tilemaps;

public class MapManager : MonoBehaviour
{
    [SerializeField] private Tilemap _tilemap;
    [SerializeField] private GameObject buildingLabelPrefab;
    [SerializeField] private List<BuildingScriptableObject> tileData;
    private readonly Dictionary<TileBase, BuildingScriptableObject> _dataFromTiles = new();

    private readonly Dictionary<Vector3Int, Building> _buildings = new();

    private Building _connectionStartBuilding;
    [SerializeField] private LineRenderer _lineRenderer;
    [SerializeField] private LineRenderer _pendingLineRenderer;

    [SerializeField] private GameObject _lineInstanceHolder;
    [SerializeField] private GameObject _buildingLabelInstanceHolder;

    private void Awake()
    {
        foreach (var buildingScriptableObject in tileData)
        {
            _dataFromTiles.Add(buildingScriptableObject.tile, buildingScriptableObject);
        }
    }

    private void PlaceBuilding(Vector3Int position, Building building)
    {
        _tilemap.SetTile(position, building.BuildingScriptableObject.tile);

        var worldPosition = new Vector3(position.x, position.y + 1, -1);
        var buildingLabelInstance = Instantiate(buildingLabelPrefab, _buildingLabelInstanceHolder.transform);
        buildingLabelInstance.transform.position = worldPosition;
        var buildingLabel = buildingLabelInstance.GetComponent<BuildingLabel>();
        buildingLabel.SetBuildingName(building.BuildingScriptableObject.buildingName);
        buildingLabel.SetProduct(building.ProductScriptableObject);
        building.position = position;
        _buildings.Add(position, building);
    }

    private bool TryConnectBuildings(Building target, Building source)
    {
        if (target._inputs.Count != 0 || source._outputs.Count != 0) return false;

        target._inputs.Add(source);
        source._outputs.Add(target);
        var cellSize = _tilemap.layoutGrid.cellSize;
        CreateConnectionLine(ConvertTilemapGridPositionToWorld(target.position) + cellSize / 2,
            ConvertTilemapGridPositionToWorld(source.position) + cellSize / 2);
        return true;
    }

    // Update is called once per frame
    void Update()
    {
        if (_connectionStartBuilding != null && _pendingLineRenderer.gameObject.activeSelf)
        {
            var cellSize = _tilemap.layoutGrid.cellSize;
            DrawPendingConnection(ConvertTilemapGridPositionToWorld(_connectionStartBuilding.position) + cellSize / 2,
                GameManager.Instance.GetBuildingTilemapMousePosition());
        }

        if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            var gridPosition = GameManager.Instance.GetBuildingTilemapMouseGridPosition();
            var activeBuilding = GameManager.Instance.GetActiveBuilding();
            if (activeBuilding != null && GameManager.Instance.TryBuy(activeBuilding.BuildingScriptableObject))
            {
                PlaceBuilding(gridPosition, activeBuilding);
                GameManager.Instance.ClearActiveBuilding();
            }
            else
            {
                var clickedTile = _tilemap.GetTile(gridPosition);
                if (!clickedTile)
                {
                    // No tile exists at this location, so we don't need to continue
                    return;
                }

                var building = _buildings[gridPosition];
                if (building == null)
                {
                    return;
                }

                if (_connectionStartBuilding == null)
                {
                    _pendingLineRenderer.gameObject.SetActive(true);
                    _connectionStartBuilding = building;
                    return;
                }

                if (ValidConnectionTarget(_connectionStartBuilding, building))
                {
                    if (TryConnectBuildings(_connectionStartBuilding, building))
                    {
                        ClearConnectionStartBuilding();
                    }
                }
                else if (ValidConnectionSource(building, _connectionStartBuilding))
                {
                    if (TryConnectBuildings(building, _connectionStartBuilding))
                    {
                        ClearConnectionStartBuilding();
                    }
                }
            }
        }

        if (Input.GetMouseButtonDown(1))
        {
            if (_connectionStartBuilding != null)
            {
                ClearConnectionStartBuilding();
            }

            if (GameManager.Instance.GetActiveBuilding() != null)
            {
                GameManager.Instance.ClearActiveBuilding();
            }
        }
    }

    private static bool ValidConnectionTarget(Building target, Building source)
    {
        return (target.BuildingScriptableObject.isWarehouse && source.BuildingScriptableObject.isFactory ||
                target.BuildingScriptableObject.isStore && source.BuildingScriptableObject.isWarehouse) &&
               target.ProductScriptableObject == source.ProductScriptableObject;
    }

    private static bool ValidConnectionSource(Building source, Building target)
    {
        return (source.BuildingScriptableObject.isWarehouse && target.BuildingScriptableObject.isFactory ||
                source.BuildingScriptableObject.isStore && target.BuildingScriptableObject.isWarehouse) &&
               target.ProductScriptableObject == source.ProductScriptableObject;
    }

    private void ClearConnectionStartBuilding()
    {
        _connectionStartBuilding = null;
        _pendingLineRenderer.SetPositions(new Vector3[] { });
        _pendingLineRenderer.gameObject.SetActive(false);
    }

    private void CreateConnectionLine(Vector3 start, Vector3 end)
    {
        var line = Instantiate(_lineRenderer, _lineInstanceHolder.transform);
        line.positionCount = 2;
        line.SetPosition(0, start);
        line.SetPosition(1, end);
    }

    private void DrawPendingConnection(Vector3 start, Vector3 end)
    {
        _pendingLineRenderer.positionCount = 2;
        _pendingLineRenderer.SetPosition(0, start);
        _pendingLineRenderer.SetPosition(1, end);
        var gridPosition = GameManager.Instance.GetBuildingTilemapMouseGridPosition();
        if (!_buildings.ContainsKey(gridPosition))
        {
            return;
        }

        var hoveredBuilding = _buildings[gridPosition];
        if (hoveredBuilding != null &&
            (ValidConnectionTarget(_connectionStartBuilding, hoveredBuilding) ||
             ValidConnectionSource(hoveredBuilding, _connectionStartBuilding)))
        {
            _pendingLineRenderer.startColor = Color.green;
            _pendingLineRenderer.endColor = Color.green;
        }
        else
        {
            _pendingLineRenderer.startColor = Color.red;
            _pendingLineRenderer.endColor = Color.red;
        }
    }

    private Vector3 ConvertTilemapGridPositionToWorld(Vector3Int position)
    {
        return _tilemap.CellToWorld(position);
    }

    public Vector3Int ConvertWorldToTilemapGridPosition(Vector3 position)
    {
        return _tilemap.WorldToCell(position);
    }

    public List<Building> GetBuildings()
    {
        return new List<Building>(_buildings.Values);
    }

    public void AddBuilding(SaveData.BuildingData saveDataBuilding)
    {
        var building = new Building(saveDataBuilding);
        var position = building.position;
        _tilemap.SetTile(position, building.BuildingScriptableObject.tile);
        var worldPosition = new Vector3(position.x, position.y + 1, -1);
        var buildingLabelInstance = Instantiate(buildingLabelPrefab, _buildingLabelInstanceHolder.transform);
        buildingLabelInstance.transform.position = worldPosition;
        var buildingLabel = buildingLabelInstance.GetComponent<BuildingLabel>();
        buildingLabel.SetBuildingName(building.BuildingScriptableObject.buildingName);
        buildingLabel.SetProduct(building.ProductScriptableObject);
        _buildings.Add(building.position, building);
    }

    public void ConnectBuildings(List<SaveData.BuildingData> saveDataBuildings)
    {
        foreach (var saveDataBuilding in saveDataBuildings)
        {
            var startingBuilding = _buildings[saveDataBuilding.gridPosition];
            if (startingBuilding == null)
            {
                Debug.Log("Starting building not loaded");
                continue;
            }

            foreach (var inputConnection in saveDataBuilding.inputConnections)
            {
                var sourceBuilding = _buildings[inputConnection];
                if (sourceBuilding == null)
                {
                    Debug.Log("Input source building not loaded");
                    continue;
                }

                TryConnectBuildings(startingBuilding, sourceBuilding);
            }

            foreach (var outputConnection in saveDataBuilding.outputConnections)
            {
                var sourceBuilding = _buildings[outputConnection];
                if (sourceBuilding == null)
                {
                    Debug.Log("Output target building not loaded");
                    continue;
                }

                TryConnectBuildings(sourceBuilding, startingBuilding);
            }
        }
    }
}