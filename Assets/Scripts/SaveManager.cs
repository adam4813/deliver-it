using System.IO;
using Eflatun.SceneReference;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SaveManager : MonoBehaviour
{
    [SerializeField] private Button loadButton;
    [SerializeField] private Button resumeButton;
    [SerializeField] private Button backButton;
    [SerializeField] private Button quitButton;
    [SerializeField] private Button saveButton;
    [SerializeField] private Button confirmButton;
    [SerializeField] private Button cancelButton;
    [SerializeField] private TMP_InputField filenameInput;
    [SerializeField] private GameObject confirmWriteWarning;

    [SerializeField] private Button saveListItemButtonPrefab;

    [SerializeField] private GameObject saveListPanel;
    [SerializeField] private SceneReference titleScene;
    [SerializeField] private SceneReference gameScene;

    private string _selectedSaveFile;
    private Button activeSaveButton;

    private void Awake()
    {
        if (!saveListPanel)
        {
            return;
        }

        var saveFiles = FileManager.ListFiles();
        foreach (var saveFile in saveFiles)
        {
            var saveListItemButton = Instantiate(saveListItemButtonPrefab, saveListPanel.transform);
            saveListItemButton.onClick.AddListener(() =>
            {
                _selectedSaveFile = saveFile;
                saveListItemButton.GetComponent<Image>().color = Color.white;
                if (activeSaveButton)
                {
                    activeSaveButton.GetComponent<Image>().color = new Color(1, 1, 1, 0.4f);
                }

                activeSaveButton = saveListItemButton;
            });
            var fileInto = new FileInfo(saveFile);
            saveListItemButton.GetComponentInChildren<TMP_Text>().text = fileInto.Name;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (quitButton)
        {
            quitButton.onClick.AddListener(ToTitleScreen);
        }

        if (backButton)
        {
            backButton.onClick.AddListener(ToTitleScreen);
        }

        if (loadButton)
        {
            loadButton.onClick.AddListener(LoadSave);
        }

        if (resumeButton)
        {
            resumeButton.onClick.AddListener(ResumeGame);
        }

        if (saveButton)
        {
            saveButton.onClick.AddListener(() =>
            {
                var saveData = new SaveData();
                GameManager.Instance.PopulateSaveData(saveData);
                WriteSave(saveData);
            });
        }

        if (cancelButton)
        {
            cancelButton.onClick.AddListener(HideConfirmWrite);
        }

        if (confirmButton)
        {
            confirmButton.onClick.AddListener(() =>
            {
                var saveData = new SaveData();
                GameManager.Instance.PopulateSaveData(saveData);
                ConfirmWriteSave(saveData);
            });
        }
    }

    private void LoadSave()
    {
        if (string.IsNullOrEmpty(_selectedSaveFile))
        {
            return;
        }

        Debug.Log($"Loading save {_selectedSaveFile}");
        if (FileManager.LoadFromFile(_selectedSaveFile, out var saveJson))
        {
            Debug.Log($"Loaded save {_selectedSaveFile}");
            var saveData = new SaveData();
            saveData.LoadFromJson(saveJson);
            GameManager.Instance.LoadFromSaveData(saveData);
            SceneManager.LoadScene(gameScene.Name);
        }
    }

    private void ConfirmWriteSave(SaveData saveData)
    {
        var filename = GetWriteSaveFileName();
        Debug.Log($"Overwriting save {filename}");
        if (FileManager.WriteToFile(filename, saveData.ToJson()))
        {
            Debug.Log($"Overwrote save {filename}");
        }
    }

    private string GetWriteSaveFileName()
    {
        return $"{filenameInput.text}.json";
    }

    private void ShowConfirmWrite()
    {
        saveButton.gameObject.SetActive(false);
        confirmWriteWarning.SetActive(true);
    }

    private void HideConfirmWrite()
    {
        confirmWriteWarning.SetActive(false);
        saveButton.gameObject.SetActive(true);
    }

    public static void AutoSave()
    {
        const string filename = "auto_save.json";
        var saveData = new SaveData();
        GameManager.Instance.PopulateSaveData(saveData);
        Debug.Log($"Writing save {filename}");
        if (FileManager.WriteToFile(filename, saveData.ToJson()))
        {
            Debug.Log($"Wrote save {filename}");
        }
    }

    public static void AutoLoad()
    {
        const string filename = "auto_save.json";
        Debug.Log($"Loading save {filename}");
        if (FileManager.LoadFromFile(filename, out var saveJson))
        {
            Debug.Log($"Loaded save {filename}");
            var saveData = new SaveData();
            saveData.LoadFromJson(saveJson);
            GameManager.Instance.LoadFromSaveData(saveData);
        }
    }

    private void WriteSave(SaveData saveData)
    {
        var filename = string.IsNullOrEmpty(_selectedSaveFile) ? GetWriteSaveFileName() : _selectedSaveFile;
        if (string.IsNullOrEmpty(_selectedSaveFile) && FileManager.FileExists(filename))
        {
            Debug.Log($"File exists {filename}");
            ShowConfirmWrite();
            return;
        }

        Debug.Log($"Writing save {filename}");
        if (FileManager.WriteToFile(filename, saveData.ToJson()))
        {
            Debug.Log($"Wrote save {filename}");
        }
    }

    private void ResumeGame()
    {
        SceneManager.LoadScene(gameScene.Name);
        GameManager.Instance.ShowMap();
        GameManager.Instance.Resume();
    }

    private void ToTitleScreen()
    {
        SceneManager.LoadScene(titleScene.Name);
    }
}