﻿using System;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct RequestListItem
{
    public ProductScriptableObject product;
    public int amount;

}

[CreateAssetMenu(fileName = "Request", menuName = "Request/Request", order = 1)]
public class RequestScriptableObject : ScriptableObject
{
    public List<RequestListItem> items;
    public int reward;
}