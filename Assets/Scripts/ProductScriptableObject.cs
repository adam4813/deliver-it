using UnityEngine;

[CreateAssetMenu(fileName = "Product", menuName = "Product/Product", order = 1)]
public class ProductScriptableObject : ScriptableObject
{
    public Sprite icon;
    public int sellCost;
}
