using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour, ISaveable
{
    private Camera _mainCamera;
    [SerializeField] private MapManager mapManager;
    [SerializeField] private float tickRate = 1;
    [SerializeField] private float requestRate = 30;
    public static GameManager Instance;
    private Building _activeToolbarBuilding;
    [SerializeField] private ActiveBuildingMouseFollow activeBuildingMouseFollow;
    private float _deltaAccumulator = 0;
    private float _requestDeltaAccumulator = 15;
    [SerializeField] private int _startinMoney = 100;
    private int _money;
    public MoneyPanel moneyPanel;
    public AvailableSupplyPanel availableSupplyPanel;
    public PauseMenu pauseMenu;
    public bool isPaused;

    public List<ProductScriptableObject> products;

    private bool builtFirstFactory;
    private bool builtFirstStore;
    private bool builtFirstWarehosue;

    private Dictionary<ProductScriptableObject, int> availableProductQuantities = new();
    public RequestPanel requestPanel;
    [SerializeField] private List<RequestScriptableObject> availableRequests;

    public List<SaveData.RequestListItemData> activeRequests = new();

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(this);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(this);
        _money = _startinMoney;
        _requestDeltaAccumulator = requestRate;
        foreach (var productScriptableObject in products)
        {
            availableProductQuantities.Add(productScriptableObject, 0);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) && !pauseMenu.isShown)
        {
            Debug.Log("Showing pause menu");
            pauseMenu.Show();
            ;
            isPaused = true;
        }

        if (!isPaused)
        {
            if (requestPanel)
            {
                _requestDeltaAccumulator += Time.deltaTime;
                if (_requestDeltaAccumulator > requestRate)
                {
                    if (activeRequests.Count < 3)
                    {
                        var request = availableRequests[Random.Range(0, availableRequests.Count)];
                        var requestListItem = new SaveData.RequestListItemData()
                        {
                            _requestScriptableObject = request,
                            index = activeRequests.Count
                        };
                        requestPanel.CreateRequest(requestListItem);
                        activeRequests.Add(requestListItem);
                        _requestDeltaAccumulator = 0;
                    }
                }
            }

            _deltaAccumulator += Time.deltaTime;
            var buildings = mapManager.GetBuildings();
            if (_deltaAccumulator > tickRate)
            {
                _deltaAccumulator -= tickRate;
                foreach (var productScriptableObject in products)
                {
                    var stores = buildings.FindAll(building =>
                        building.BuildingScriptableObject.isStore &&
                        building.ProductScriptableObject == productScriptableObject);
                    int totalSupply = 0;
                    foreach (var building in stores)
                    {
                        //building.SellProducts();
                        building.ReceiveProduct();
                        building.RequestProduct();
                        totalSupply += building.GetSupply();
                    }

                    availableProductQuantities[productScriptableObject] = totalSupply;
                }

                var warehouses = buildings.FindAll(building => building.BuildingScriptableObject.isWarehouse);
                foreach (var building in warehouses)
                {
                    building.ReceiveProduct();
                    building.RequestProduct();
                }

                var factories = buildings.FindAll(building => building.BuildingScriptableObject.isFactory);
                foreach (var building in factories)
                {
                    building.MakeProduct();
                }
            }
        }
    }

    public void SetActiveBuilding(BuildingScriptableObject buildingScriptableObjects,
        ProductScriptableObject productScriptableObject)
    {
        _activeToolbarBuilding = new Building(buildingScriptableObjects, productScriptableObject);
        activeBuildingMouseFollow.SetImage(buildingScriptableObjects.image);
        SetActiveBuildingMouseFollowStatus();
    }

    private void SetActiveBuildingMouseFollowStatus()
    {
        if (_activeToolbarBuilding.BuildingScriptableObject.cost <= _money)
        {
            activeBuildingMouseFollow.SetValid();
        }
        else
        {
            activeBuildingMouseFollow.SetInvalid();
        }
    }

    public void ClearActiveBuilding()
    {
        _activeToolbarBuilding = null;
        activeBuildingMouseFollow.ClearImage();
    }

    public Building GetActiveBuilding()
    {
        return _activeToolbarBuilding;
    }

    public Vector3Int GetBuildingTilemapMouseGridPosition()
    {
        var worldMousePosition = GetBuildingTilemapMousePosition();
        return mapManager.ConvertWorldToTilemapGridPosition(worldMousePosition);
    }

    public Vector3 GetBuildingTilemapMousePosition()
    {
        return _mainCamera ? _mainCamera.ScreenToWorldPoint(Input.mousePosition) : new Vector3();
    }

    public void ChangeMoney(int amount)
    {
        _money += amount;
        if (moneyPanel)
        {
            moneyPanel.SetMoney(_money);
        }

        if (_activeToolbarBuilding != null)
        {
            SetActiveBuildingMouseFollowStatus();
        }
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
        SceneManager.sceneUnloaded += OnSceneUnloaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
        SceneManager.sceneUnloaded -= OnSceneUnloaded;
    }

    private void OnApplicationQuit()
    {
        //SaveManager.AutoSave();
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        _mainCamera = Camera.main;
    }

    private void OnSceneUnloaded(Scene scene)
    {
        //SaveManager.AutoSave();
    }

    public void PopulateSaveData(SaveData saveData)
    {
        saveData.money = _money;
        var saveDataBuildings = new List<SaveData.BuildingData>();
        foreach (var building in mapManager.GetBuildings())
        {
            saveDataBuildings.Add(building.ToSaveData());
        }

        saveData.buildings = saveDataBuildings;

        var saveDataRequests = new List<SaveData.RequestListItemData>();
        for (var i = 0; i < activeRequests.Count; i++)
        {
            saveDataRequests.Add(new SaveData.RequestListItemData()
            {
                index = i,
                _requestScriptableObject = activeRequests[i]._requestScriptableObject
            });
        }

        saveData.requests = saveDataRequests;
    }

    public void LoadFromSaveData(SaveData saveData)
    {
        _money = saveData.money;
        foreach (var saveDataBuilding in saveData.buildings)
        {
            mapManager.AddBuilding(saveDataBuilding);
        }

        mapManager.ConnectBuildings(saveData.buildings);

        activeRequests = saveData.requests;
    }

    public int GetMoney()
    {
        return _money;
    }

    public void ShowMap()
    {
        mapManager.gameObject.SetActive(true);
    }

    public void HideMap()
    {
        mapManager.gameObject.SetActive(false);
    }

    public void Resume()
    {
        isPaused = false;
    }

    public bool TryBuy(BuildingScriptableObject buildingScriptableObject)
    {
        if (!builtFirstFactory && buildingScriptableObject.isFactory)
        {
            builtFirstFactory = true;
            return true;
        }

        if (!builtFirstStore && buildingScriptableObject.isStore)
        {
            builtFirstStore = true;
            return true;
        }

        if (!builtFirstWarehosue && buildingScriptableObject.isWarehouse)
        {
            builtFirstWarehosue = true;
            return true;
        }

        if (buildingScriptableObject.cost > _money)
        {
            return false;
        }

        ChangeMoney(-buildingScriptableObject.cost);
        return true;
    }

    public int GetAvailableProductQuantity(ProductScriptableObject productScriptableObject)
    {
        return availableProductQuantities[productScriptableObject];
    }

    public bool TryCompleteRequest(RequestScriptableObject requestScriptableObject)
    {
        foreach (var requestListItem in requestScriptableObject.items)
        {
            Debug.Log($"trying to complete request item {requestListItem.product} for amount{requestListItem.amount}");
            if (availableProductQuantities[requestListItem.product] < requestListItem.amount)
            {
                return false;
            }
        }
        var buildings = mapManager.GetBuildings();
        foreach (var requestListItem in requestScriptableObject.items)
        {
            var stores = buildings.FindAll(building =>
                building.BuildingScriptableObject.isStore &&
                building.ProductScriptableObject == requestListItem.product);
            var amountPerBuilding = requestListItem.amount / stores.Count;
            var overflow = 0;
            if (amountPerBuilding * stores.Count != requestListItem.amount)
            {
                overflow += 1;
            }
            foreach (var building in stores)
            {
                var buildingSupply = building.GetSupply();
                if (buildingSupply < amountPerBuilding)
                {
                    overflow += amountPerBuilding - buildingSupply;
                    building.RemoveProducts(buildingSupply);
                }
                else
                {
                    var extraBuildingSupply = buildingSupply - amountPerBuilding;
                    overflow -= extraBuildingSupply;
                    building.RemoveProducts(amountPerBuilding + extraBuildingSupply);
                }
            }
        }
        
        ChangeMoney(requestScriptableObject.reward);

        return true;
    }

    public void RemoveRequest(SaveData.RequestListItemData requestListItemData)
    {
        activeRequests.RemoveAt(requestListItemData.index);
        var i = 0;
        activeRequests.ForEach(data => data.index = i++);
    }
}