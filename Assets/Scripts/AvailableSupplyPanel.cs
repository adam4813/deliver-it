using UnityEngine;

public class AvailableSupplyPanel : MonoBehaviour
{
    [SerializeField] private GameObject availableSupplyPrefab;
    
    private void Start()
    {
        GameManager.Instance.availableSupplyPanel = this;
        foreach (var productScriptableObject in GameManager.Instance.products)
        {
            var availableSupplyInstance = Instantiate(availableSupplyPrefab, transform);
            var availableSupplySlot = availableSupplyInstance.GetComponent<AvailableSupplySlot>();
            availableSupplySlot.SetProduct(productScriptableObject);
            
        }
    }

    private void OnDestroy()
    {
        if (GameManager.Instance.availableSupplyPanel == this)
        {
            GameManager.Instance.availableSupplyPanel = null;
        }
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
