using UnityEngine;
using UnityEngine.UI;
using Eflatun.SceneReference;
using UnityEngine.SceneManagement;

public class TitleScreen : MonoBehaviour
{
    [SerializeField] private Button newButton;
    [SerializeField] private Button loadButton;
    [SerializeField] private Button quitButton;
    
    [SerializeField] private SceneReference gameScreen;
    [SerializeField] private SceneReference loadScreen;

    // Start is called before the first frame update
    void Start()
    {
        newButton.onClick.AddListener(ToGameScreen);
        loadButton.onClick.AddListener(ToLoadScreen);
        quitButton.onClick.AddListener(Application.Quit);
    }
    private void ToGameScreen()
    {
        SceneManager.LoadScene(gameScreen.Name);
    }
    private void ToLoadScreen()
    {
        SceneManager.LoadScene(loadScreen.Name);
    }
}